// HTML
var ID_CONTAINER_TABLE = "table";

var ID_CONTAINER_AUDIO = "audio";

var ID_NEW_GAME = "newGame";

var ID_WIN = "win";

var ID_LOSE = "lose";

var ID_SETUP = "setup";


// IMAGES

var FOLDER_IMAGES = 'images';

var SRC_IMAGE_BEFORE = "icon_before.png";

var SRC_IMAGE_AFTER = "icon_after.png";

var SRC_IMAGE_BOMB_BEFORE = "bomb_icon_before.png";

var SRC_IMAGE_BOMB_INACTIVE = "icon_bomb_inactive.png";

var SRC_IMAGE_BOMB_ACTIVE = "icon_bomb_active.jpg";

var SRC_IMAGE_TROPHY = "trophy_icon.png";


// AUDIO

var FOLDER_MP3 = 'mp3';

var SRC_WINNER_SONG = FOLDER_MP3 + "/" + "win.mp3";

var SRC_LOSER_SONG = FOLDER_MP3 + "/" + "lose.mp3";