// GAME BUILD
function buildTable () {

	var output = '<table cellpadding="5" align="center">';

	for (var i = 0; i < ROW_NUMBER; i++) { 
		output += buildRow();
	}

	output += '</table>';

	var table = getContainerTable();
	table.innerHTML = output;
};

function buildRow () {

	var output = '<tr>';

	for (var i = 0; i < COLUMN_NUMBER; i++) { 
		output += buildColumn();
	}

	output += '</tr>';

	return output;
};

function buildColumn () {

	var output = '<td onclick="cellClick(this);">';

	output += '<img src="' + FOLDER_IMAGES + '/' + SRC_IMAGE_BEFORE + '">';

	output += '</td>';

	return output;	
};

// implanta as bombas (de maneira randomica) baseado no número de colunas e linhas
function releaseBombs () {

	while (implantedBombas < TOTAL_BOMBS) {

		var row = Math.floor(Math.random() * ROW_NUMBER);
		var column = Math.floor(Math.random() * COLUMN_NUMBER);

		var element = getElementFromTable(row, column);

		if (!hasBomb(element)) {

			element.children[0].src = FOLDER_IMAGES + '/' + SRC_IMAGE_BOMB_BEFORE;

			implantedBombas++;
		}
	}
};