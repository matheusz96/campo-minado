/* Entrada inicial */
function main () {

	config();

	init();
};

/* Configura botões */
function config () {

	var newGameElement = document.getElementById(ID_NEW_GAME);
	newGameElement.onclick = function () {
		init();
	};

	var winElement = document.getElementById(ID_WIN);
	winElement.onclick = function () {
		gameWon();
	};

	var loseElement = document.getElementById(ID_LOSE);
	loseElement.onclick = function () {
		gameOver();
	};
};

/* Centraliza a lógica para início do jogo */
function init () {

	clean();

	buildTable();

	releaseBombs();

	start();
};

function clean () {

	var containerTableElement = getContainerTable();
	containerTableElement.innerHTML = '';

	var containerAudioElement = getAudioContainer();
	containerAudioElement.innerHTML = '';

	// total de bombas já implantadas
	implantedBombas = 0;

	// Total de células sem bombas. Utilizada para saber quando o jogador ganhou o jogo.
	emptyCell = (COLUMN_NUMBER * ROW_NUMBER) - TOTAL_BOMBS;

	// status da partida
	ready();
};