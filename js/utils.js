// UTILS

function getContainerTable () {
	return document.getElementById(ID_CONTAINER_TABLE);
};

function getRow (row) {
	return getContainerTable().children[0].children[0].children[row];
};

function getElementFromTable (row, column) {
	return getRow(row).children[column];
};

function hasBomb (element) {
	return element.children[0].src.indexOf(SRC_IMAGE_BOMB_BEFORE) > -1 ? true : false;
};

function isFirstClick (src) {
	return src.indexOf(SRC_IMAGE_BEFORE) > -1 ? true : false;
};

function getAudioContainer () {
	return document.getElementById(ID_CONTAINER_AUDIO);
};

function isStarted () {
	return status_game === 'IN PROGRESS';
};