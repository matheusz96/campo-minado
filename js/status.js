// GAME STATUS

function ready () {
	status_game = 'READY';
};

function start () {
	status_game = 'IN PROGRESS';
};

function end () {
	status_game = 'FINISHED';
};