// GAME ENGINE

function cellClick(element) {

	if (!isStarted()) {
		return;
	}

	if (isFirstClick(element.children[0].src)) {

		if (hasBomb(element)) {
			element.children[0].src = FOLDER_IMAGES + '/' + SRC_IMAGE_BOMB_ACTIVE;
			gameOver();
		
		} else {
			element.children[0].src = FOLDER_IMAGES + '/' + SRC_IMAGE_AFTER;
			emptyCell--;
		}

		if (emptyCell === 0) {
			gameWon();
		}
	}
};

function gameWon () {

	if (!isStarted()) {
		return;
	}

	showTrophy();

	playWinnerSong();

	end();

};

function gameOver () {

	if (!isStarted()) {
		return;
	}

	showBombs();

	playLoserSong();

	end();
};

function showBombs () {
	for (var row = 0; row < ROW_NUMBER; row++) { 

		for (var column = 0; column < COLUMN_NUMBER; column++) {

			var element = getElementFromTable(row, column);
		
			if (hasBomb(element)) {
				element.children[0].src = FOLDER_IMAGES + '/' + SRC_IMAGE_BOMB_INACTIVE;
			}			
		}
	}

};

function playWinnerSong () {
		var element = getAudioContainer();
		element.innerHTML = "<embed src='" + SRC_WINNER_SONG + "' hidden=true autostart=true loop=false>";
};

function playLoserSong () {
		var element = getAudioContainer();
		element.innerHTML = "<embed src='" + SRC_LOSER_SONG + "' hidden=true autostart=true loop=false>";
};

function showTrophy () {

	for (var row = 0; row < ROW_NUMBER; row++) { 

		for (var column = 0; column < COLUMN_NUMBER; column++) {

			var element = getElementFromTable(row, column);
		
			if (!hasBomb(element)) {
				element.children[0].src = FOLDER_IMAGES + '/' + SRC_IMAGE_TROPHY;
			}			
		}
	}
};